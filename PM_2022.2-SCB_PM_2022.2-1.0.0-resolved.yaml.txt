---
openapi: 3.0.0
info:
  title: Simple Inventory API
  description: This is a simple API
  contact:
    email: you@your-company.com
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
- url: https://virtserver.swaggerhub.com/PM_2022.2/SCB_PM_2022.2/1.0.0
  description: SwaggerHub API Auto Mocking
- url: https://virtserver.swaggerhub.com/hvictorleite/PM_SCB/1.0.0
  description: SwaggerHub API Auto Mocking
tags:
- name: Ciclista
- name: Funcionario
- name: Bicicletario
- name: Pagamento
- name: Email
paths:
  /ciclista:
    post:
      tags:
      - Ciclista
      summary: Cadastrar ciclista
      description: |
        Cadastrar um ciclista no sistema
      operationId: cadastrarCiclista
      requestBody:
        description: Ciclista a ser cadastrado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Ciclista'
      responses:
        "201":
          description: Ciclista cadastrado com sucesso.
        "400":
          description: Dados inválidos
  /ciclista/validacaocartao:
    post:
      tags:
      - Ciclista
      summary: Validar cartão de crédito
      description: Validar o número de cartão de crédito fornecido pelo individuo.
      operationId: validarCartao
      requestBody:
        description: Cartão a ser validado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CartaoCredito'
      responses:
        "200":
          description: Cartão validado com sucesso.
        "400":
          description: Cartão inválido
  /ciclista/{cpf}:
    get:
      tags:
      - Ciclista
      summary: Listar funcionários
      description: Apresenta os dados de um determinado ciclista cadastrado no sistema
      operationId: getCiclista
      parameters:
      - name: cpf
        in: path
        description: Número do id do ciclista para consultar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Ciclista encontrado
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Ciclista'
        "404":
          description: Ciclista não encontrado.
    put:
      tags:
      - Ciclista
      summary: Atualizar um ciclista existente
      description: Atualiza um ciclista cadastrado no sistema
      operationId: atualizarCiclista
      parameters:
      - name: cpf
        in: path
        description: Número do id do ciclista a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Ciclista a ser atualizado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Ciclista'
      responses:
        "200":
          description: Ciclista atualizado com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Ciclista não encontrado
    patch:
      tags:
      - Ciclista
      summary: Atualizar o status de um ciclista
      description: Atualiza o status de um ciclista em processo de cadastro
      operationId: atualizarStatusCiclista
      parameters:
      - name: cpf
        in: path
        description: Número do id do ciclista a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Status'
      responses:
        "200":
          description: Ciclista atualizado com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Ciclista não encontrado
  /funcionario:
    get:
      tags:
      - Funcionario
      summary: Listar funcionários
      description: |
        Lista todos os funcionários cadastrados no sistema
      operationId: getFuncionarios
      responses:
        "200":
          description: Resultados da busca com sucesso de todos os funcionários do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Funcionario'
        "404":
          description: Não foram encontrados funcionários no sistema
    post:
      tags:
      - Funcionario
      summary: Cadastrar funcionário
      requestBody:
        description: Funcionário a ser cadastrado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Funcionario'
      responses:
        "201":
          description: Funcionário cadastrado com sucesso.
  /funcionario/{matricula}:
    get:
      tags:
      - Funcionario
      summary: Consultar funcionário
      description: |
        Apresenta os dados de um determinado funcionário cadastrado no sistema
      operationId: getFuncionario
      parameters:
      - name: matricula
        in: path
        description: Número da matrícula do funcionário a ser consultado
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Funcionário encontrado
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Funcionario'
        "404":
          description: Funcionário não encontrado
    put:
      tags:
      - Funcionario
      summary: Atualizar um funcionário existente
      description: |
        Atualiza uma funcionário cadastrado no sistema
      operationId: atualizarFuncionario
      parameters:
      - name: matricula
        in: path
        description: Número da matrícula do funcionário a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Funcionario a ser atualizado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Funcionario'
      responses:
        "200":
          description: Funcionario atualizado com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Funcionario não encontrado
    delete:
      tags:
      - Funcionario
      summary: Remover uma funcionário existente
      description: |
        Remove um funcionário do sistema mediante o identificador único.
      operationId: removerFuncionario
      parameters:
      - name: matricula
        in: path
        description: Matrícula do funcionário a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Funcionário removido com sucesso
        "404":
          description: Funcionário não encontrado
  /bicicletario/bicicleta:
    get:
      tags:
      - Bicicletario
      summary: Listar bicicletas
      description: Lista todas as bicicletas cadastradas no sistema
      operationId: getBicicletas
      responses:
        "200":
          description: Resultados da busca com sucesso de todas as bicicletas do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Bicicleta'
        "404":
          description: Não foram encontradas bicicletas no sistema
    post:
      tags:
      - Bicicletario
      summary: Cadastrar bicicleta
      requestBody:
        description: Bicicleta a ser cadastrada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Bicicleta'
      responses:
        "201":
          description: Bicicleta cadastrada com sucesso.
  /bicicletario/bicicleta/{numero}:
    get:
      tags:
      - Bicicletario
      summary: Consultar bicicleta
      description: |
        Consulta determinada bicicleta cadastrada no sistema
      operationId: getBicicleta
      parameters:
      - name: numero
        in: path
        description: Número da bicicleta a consultar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Resultado dos dados da bicicleta resgatados com sucesso
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Bicicleta'
        "404":
          description: Não foram encontradas bicicletas no sistema
    put:
      tags:
      - Bicicletario
      summary: Atualizar uma bicicleta existente
      description: Atualiza uma bicicleta do sistema mediante o identificador único.
      operationId: atualizarBicicleta
      parameters:
      - name: numero
        in: path
        description: Número da bicicleta a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Bicicleta a ser atualizada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Bicicleta'
      responses:
        "200":
          description: Bicicleta atualizada com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Bicicleta não encontrada
    delete:
      tags:
      - Bicicletario
      summary: Remover uma bicicleta existente
      description: Altera o status de uma bicicleta para 'excluída'  do sistema mediante o identificador único.
      operationId: removerBicicleta
      parameters:
      - name: numero
        in: path
        description: Número da bicicleta a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Bicicleta removida com sucesso
        "404":
          description: Bicicleta não encontrada
    patch:
      tags:
      - Bicicletario
      summary: Atualizar o status de uma bicicleta
      description: Atualiza o status de uma bicicleta
      operationId: atualizarStatusBicleta
      parameters:
      - name: numero
        in: path
        description: Número do id da bicicleta a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Status'
      responses:
        "200":
          description: Status da bicicleta atualizado com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Bicicleta não encontrada
  /bicicletario/tranca:
    get:
      tags:
      - Bicicletario
      summary: Listar trancas
      description: |
        Lista todas as trancas cadastradas no sistema
      operationId: getTrancas
      responses:
        "200":
          description: Resultados da busca com sucesso de todas as trancas do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Tranca'
        "404":
          description: Não foram encontradas trancas no sistema
    post:
      tags:
      - Bicicletario
      summary: Cadastrar tranca
      requestBody:
        description: Tranca a ser cadastrada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tranca'
      responses:
        "201":
          description: Tranca cadastrada com sucesso.
  /bicicletario/tranca/{numero}:
    get:
      tags:
      - Bicicletario
      summary: Consultar tranca
      description: |
        Consulta determinada tranca cadastrada no sistema
      operationId: getTranca
      parameters:
      - name: numero
        in: path
        description: Número da tranca a consultar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Resultado dos dados da tranca resgatados com sucesso
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Tranca'
        "404":
          description: Não foram encontrada a tranca no sistema
    put:
      tags:
      - Bicicletario
      summary: Atualizar uma tranca existente
      description: |
        Atualiza uma tranca cadastrada no sistema
      operationId: atualizarTranca
      parameters:
      - name: numero
        in: path
        description: Número da tranca a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Tranca a ser atualizada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tranca'
      responses:
        "200":
          description: Tranca atualizada com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Tranca não encontrada
    delete:
      tags:
      - Bicicletario
      summary: Remover uma tranca existente
      description: Altera o status de uma tranca para 'excluída' do sistema mediante o identificador único.
      operationId: removerTranca
      parameters:
      - name: numero
        in: path
        description: Número da tranca a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Tranca removida com sucesso
        "404":
          description: Tranca não encontrada
    patch:
      tags:
      - Bicicletario
      summary: Atualizar o status de uma tranca
      description: Atualiza o status de uma tranca
      operationId: atualizarStatusTranca
      parameters:
      - name: numero
        in: path
        description: Número do id da tranca a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Tranca atualizada com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Tranca não encontrada
  /bicicletario/totem:
    get:
      tags:
      - Bicicletario
      summary: Listar totens
      description: |
        Lista todos os totens cadastrados no sistema
      operationId: getTotens
      responses:
        "200":
          description: Resultados da busca com sucesso de todos os totens do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Totem'
        "404":
          description: Não foram encontrados totens no sistema
    post:
      tags:
      - Bicicletario
      summary: Cadastrar totem
      requestBody:
        description: Totem a ser cadastrado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Totem'
      responses:
        "201":
          description: Totem cadastrado com sucesso.
  /bicicletario/totem/{numero}:
    delete:
      tags:
      - Bicicletario
      summary: Remover uma totem existente
      description: |
        Remove um totem do sistema mediante o identificador único.
      operationId: removerTotem
      parameters:
      - name: numero
        in: path
        description: Número do totem a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Totem removido com sucesso
        "404":
          description: Totem não encontrado
  /bicicletario/totem/{numero}/bicicletas:
    get:
      tags:
      - Bicicletario
      summary: Listar bicicletas de um totem
      description: |
        Lista todos as bicicletas cadastradas em um totem
      operationId: getBicicletasTotem
      parameters:
      - name: numero
        in: path
        description: Número do totem em que se deve cadastrar a bicicleta
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Resultados da busca com sucesso de todas as bicicletas deste totem
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Bicicleta'
        "404":
          description: Não foram encontrados totens no sistema
    post:
      tags:
      - Bicicletario
      summary: Cadastrar bicicleta em um determinado totem
      parameters:
      - name: numero
        in: path
        description: Número do totem em que se deve cadastrar a bicicleta
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Bicicleta a ser cadastrada no totem.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Bicicleta'
      responses:
        "201":
          description: Bicicleta cadastrada no totem com sucesso.
  /bicicletario/totem/{numero}/trancas:
    get:
      tags:
      - Bicicletario
      summary: Listar trancas de um totem
      description: |
        Lista todos as trancas cadastradas em um totem
      operationId: getTrancasTotem
      parameters:
      - name: numero
        in: path
        description: Número do totem da lista de trancas
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "200":
          description: Resultados da busca com sucesso de todas as trancas deste totem
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Tranca'
        "404":
          description: Não foram encontradas trancas no totem
    post:
      tags:
      - Bicicletario
      summary: Cadastrar tranca em um determinado totem
      parameters:
      - name: numero
        in: path
        description: Número do totem em que se deve cadastrar a tranca
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Tranca a ser cadastrada no totem.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tranca'
      responses:
        "201":
          description: Tranca cadastrada no totem com sucesso.
  /pagamento:
    post:
      tags:
      - Pagamento
      summary: Descontar pagamento
      requestBody:
        description: Pagamento a ser descontado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Pagamento'
      responses:
        "200":
          description: Pagamento realizado com sucesso
  /email/enviar:
    post:
      tags:
      - Email
      summary: Enviar e-mail
      requestBody:
        description: E-mail e mensagem a serem enviados.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
      responses:
        "200":
          description: E-mail enviado com sucesso.
        "400":
          description: Não foi possível enviar o e-mail.
  /email/confirmar:
    post:
      tags:
      - Email
      summary: Enviar e-mail
      requestBody:
        description: E-mail e mensagem a serem enviados.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
      responses:
        "200":
          description: E-mail confirmado.
  /email/validar:
    post:
      tags:
      - Email
      summary: Validar e-mail
      requestBody:
        description: E-mail a ser validado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Email'
      responses:
        "200":
          description: ' O e-mail é válido'
        "400":
          description: O e-mail é inválido
components:
  schemas:
    Ciclista:
      required:
      - cartaoCredito
      - cpf
      - email
      - fotoDocumento
      - indicadorBrasileiroOuEstrangeiro
      - nome
      - passaporte
      - senha
      type: object
      properties:
        email:
          type: string
          format: Email
          example: joaocandido@email.com
        nome:
          type: string
          example: João Cândido
        indicadorBrasileiroOuEstrangeiro:
          type: boolean
        cpf:
          type: string
          example: 000.000.000-00
        passaporte:
          $ref: '#/components/schemas/Passaporte'
        senha:
          type: string
          format: password
          example: $2a$11$MXOOO1JYngri2arcL6Cic.KuBujhqgz.B2ri6szqN2/cfsdiQa7se
        cartaoCredito:
          $ref: '#/components/schemas/CartaoCredito'
        fotoDocumento:
          type: string
          format: url
          example: https://www.aws.com/image
    Passaporte:
      required:
      - dataValidade
      - pais
      - passaporte
      type: object
      properties:
        passaporte:
          type: string
          example: CS265436
        dataValidade:
          type: string
          format: date
          example: 2016-08-29
        pais:
          type: string
          example: Brazil
    CartaoCredito:
      required:
      - codigoSeguranca
      - nome
      - numero
      - validade
      type: object
      properties:
        numero:
          type: string
          example: 4444-4444-4444-4444
        nome:
          type: string
          example: João Cândido
        validade:
          type: string
          format: date
        codigoSeguranca:
          type: string
          example: "123"
    Bicicleta:
      required:
      - ano
      - marca
      - modelo
      - numero
      - status
      type: object
      properties:
        numero:
          type: integer
          example: 1
        marca:
          type: string
          example: Hondiz
        modelo:
          type: string
          example: Urban
        ano:
          type: string
          format: year
          example: YYYY
        status:
          type: string
          format: enum
          example: Nova
    Funcionario:
      required:
      - cpf
      - email
      - funcao
      - idade
      - matricula
      - nome
      - senha
      type: object
      properties:
        matricula:
          type: integer
          example: 1
        senha:
          type: string
          format: password
          example: $2a$11$MXOOO1JYngri2arcL6Cic.KuBujhqgz.B2ri6szqN2/cfsdiQa7se
        nome:
          type: string
          example: João Cândido
        idade:
          type: integer
          example: 40
        funcao:
          type: string
          example: reparador
        cpf:
          type: string
          example: 000.000.000-00
        email:
          type: string
          format: email
          example: joaocandido@email.com
    Tranca:
      required:
      - anoFabricacao
      - modelo
      - numero
      - status
      type: object
      properties:
        numero:
          type: integer
          example: 1
        status:
          type: string
          format: enum
          example: Nova
        anoFabricacao:
          type: string
          format: year
          example: YYYY
        modelo:
          type: string
          example: SuperBlock
    Totem:
      required:
      - descricao
      - listaBicicletas
      - listaTrancas
      - numero
      type: object
      properties:
        numero:
          type: integer
          example: 1
        descricao:
          type: string
        listaBicicletas:
          type: array
          items:
            $ref: '#/components/schemas/Bicicleta'
        listaTrancas:
          type: array
          items:
            $ref: '#/components/schemas/Tranca'
    Pagamento:
      required:
      - CartaoCredito
      - cpf
      - valor
      type: object
      properties:
        CartaoCredito:
          $ref: '#/components/schemas/CartaoCredito'
        cpf:
          type: string
          example: 000.000.000-00
        valor:
          type: number
          example: 200
    Email:
      required:
      - email
      - mensagem
      type: object
      properties:
        email:
          type: string
          format: Email
          example: joaocandido@email.com
        mensagem:
          type: string
          example: Você foi cadastrado com sucesso!
    Status:
      type: string
      format: Enum
      example: Ativo
